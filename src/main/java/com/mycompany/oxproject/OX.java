/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxproject;

import java.util.Scanner;

/**
 *
 * @author hanam
 */
public class OX {
    public static void main(String[] args) {
        Scanner kb= new Scanner(System.in);
        char[][] board = {{'-','-','-',},
                                  {'-','-','-',},
                                  {'-','-','-',}
                                };
        printWelcom();
        int Game = 0;
        getBoard(board);
        while(true){
            if (Game == 0) {
                printTurn();
                char sysmbol = kb.next().charAt(0);
                chooseOorX(sysmbol, kb, board);
                Game = checkWin(board, Game);
            }else{
                break;
            }
        }
    }

    public static void printWelcom() {
        System.out.println("Welcome to OX Game");
    }

    public static int checkWin(char[][] board, int Game) {
        //Vertical
        boolean c1 = (board[0][0] == 'O' && board[1][0] == 'O') && board[2][0] == 'O';
        boolean c2 = (board[0][1] == 'O' && board[1][1] == 'O') && board[2][1] == 'O';
        boolean c3 = (board[0][2] == 'O' && board[1][2] == 'O') && board[2][2] == 'O';
        //Horizontal
        boolean r1 = (board[0][0] == 'O' && board[0][1] == 'O') && board[0][2] == 'O';
        boolean r2 = (board[1][0] == 'O' && board[1][1] == 'O') && board[1][2] == 'O';
        boolean r3 = (board[2][0] == 'O' && board[2][1] == 'O') && board[2][2] == 'O';
        //Digonal
        boolean cr1 = (board[0][0] == 'O' && board[1][1] == 'O') && board[2][2] == 'O';
        boolean cr2 = (board[0][2] == 'O' && board[1][1] == 'O') && board[2][0] == 'O';
        //Lost
        boolean rx1 = (board[0][0] == 'X' && board[1][0] == 'X') && board[2][0] == 'X';
        if (c1 == true) {
            System.out.println(">>>O Win<<<");
            Game = 1;
        } else if (c2 == true) {
            System.out.println(">>>O Win<<<");
            Game = 1;
        } else if (c3 == true) {
            System.out.println(">>>O Win<<<");
            Game = 1;
        } else if (r1 == true) {
            System.out.println(">>>O Win<<<");
            Game = 1;
        } else if (r2 == true) {
            System.out.println(">>>O Win<<<");
            Game = 1;
        } else if (r3 == true) {
            System.out.println(">>>O Win<<<");
            Game = 1;
        } else if (cr1 == true) {
            System.out.println(">>>O Win<<<");
            Game = 1;
        } else if (cr2 == true) {
            System.out.println(">>>O Win<<<");
            Game = 1;
        } else if (rx1 == true) {
            System.out.println(">>>X Win<<<");
            Game = 1;
        }else {
            int mess = 0;
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[i].length; j++) {
                    if (board[i][j] == 'O' || board[i][j] == 'X') {
                        mess++;
                    }
                    if (mess == 9) {
                        Game = 1;
                        System.out.println(">>>Draw<<<");
                    }
                }
            }
        }
        return Game;
    }

    public static void chooseOorX(char sysmbol, Scanner kb, char[][] board) {
        if (sysmbol == 'X') {
            printRowCol();
            choosePosition(kb, board, sysmbol);
        } else if (sysmbol == 'O') {
            printRowCol();
            choosePosition(kb, board, sysmbol);
        }
    }

    public static void choosePosition(Scanner kb, char[][] board, char sysmbol) {
        int row = kb.nextInt();
        int column = kb.nextInt();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (i == row - 1 && j == column - 1) {
                    System.out.print(board[i][j] = sysmbol);
                } else {
                    System.out.print(board[i][j]);
                }
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    public static void printTurn() {
        System.out.println("Turn");
    }

    public static void printRowCol() {
        System.out.println("Please input row, col:");
    }
    static boolean getBoard(char[][] a) { //Call to board
        char[][] board = a;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
        return true;
    }
}
